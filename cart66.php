<!-- javascript goes in your head -->
<script type="text/javascript">
    jQuery(document).ready(function() {
	var menu_link = "#menu-item-3842"; // change this to grab the current quantity from your cart link.
        var link = jQuery("#cart-tot").text();
        var total = jQuery(menu_link).text();
        jQuery(menu_link+' a').text(total+' '+link);
        jQuery('#cart-tot').css('display', 'none');
    });
</script>

<!-- Place this where you would like your cart quantity to show -->
<span id="cart-tot">
 
	(<?php
        $items = Cart66Session::get('Cart66Cart')->countItems();
        echo ($items > 0) ? $items : '0'; 
	?>)
 
</span>
